//
//  AppDelegate.h
//  BCC_TestProject
//
//  Created by lyb on 2018/11/14.
//  Copyright © 2018年 lyb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

