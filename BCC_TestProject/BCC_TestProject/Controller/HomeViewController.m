//
//  HomeViewController.m
//  BCC_TestProject
//
//  Created by lyb on 2018/11/14.
//  Copyright © 2018年 lyb. All rights reserved.
//

#import "HomeViewController.h"
#import "HomeCell.h"

@interface HomeViewController ()
<UITableViewDelegate,
UITableViewDataSource>
@property (nonatomic, strong) UITableView * tableView;
@property (nonatomic, assign) int currPage;
@property (nonatomic, strong) NSMutableArray <Cards *>* dataArray;
@property (nonatomic, strong) NSMutableDictionary * sortedDic;
@property (nonatomic, strong) NSMutableArray * keys;

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
    [self.tableView.mj_header beginRefreshing];
}

- (void)setupUI
{
    [self.view addSubview:self.tableView];
    __weak typeof(self) weakSelf = self;
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        weakSelf.currPage = 0;
        [weakSelf prepareData];
    }];
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        weakSelf.currPage++;
        [weakSelf prepareData];
    }];
}

#pragma mark - data
- (void)prepareData
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.detailsLabelText = @"正在加载...";
    [NetWork getTestDataWithPage:self.currPage size:100 success:^(id  _Nonnull responseBody) {
        
        [hud hide:YES];
        TestModel * model = [TestModel whc_ModelWithJson:responseBody];
        if (self.currPage == 0) {
            [self.dataArray removeAllObjects];
        }
        [self.tableView.mj_header endRefreshing];
        if (model.data.count < 100) {
            [self.tableView.mj_footer endRefreshingWithNoMoreData];
        }else {
            [self.tableView.mj_footer endRefreshing];
        }
        if (model.data.cards.count != 0) {
            [self.dataArray addObjectsFromArray:model.data.cards];
        }
        [self sortArray];
        [self.tableView reloadData];
        
    } fail:^(NSError * _Nonnull error) {
        hud.detailsLabelText = error.localizedDescription;
        [hud hide:YES afterDelay:1];
    }];
}

- (void)sortArray
{
    [self.sortedDic removeAllObjects];
    [self.keys removeAllObjects];
    [self.dataArray enumerateObjectsUsingBlock:^(Cards * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        NSString * key = [obj.lastName yb_sortByFirstChinese];
        
        NSMutableArray * arr = [NSMutableArray array];
        if ([self.keys containsObject:key]) {
            [arr addObjectsFromArray:self.sortedDic[key]];
        }else {
            [self.keys addObject:key];
        }
        [arr addObject:obj];
        [self.sortedDic setObject:arr forKey:key];
        
    }];
    
    //排序
    [self.keys sortUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        return [obj1 compare:obj2 options:NSNumericSearch];
    }];
}


#pragma mark - lazyloads
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [UIView new];
        
        [_tableView registerNib:[UINib nibWithNibName:@"HomeCell" bundle:nil] forCellReuseIdentifier:homeCellId];
    }
    return _tableView;
}

- (NSMutableArray<Cards *> *)dataArray
{
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

- (NSMutableDictionary *)sortedDic
{
    if (!_sortedDic) {
        _sortedDic = [NSMutableDictionary dictionary];
    }
    return _sortedDic;
}

- (NSMutableArray *)keys
{
    if (!_keys) {
        _keys = [NSMutableArray array];
    }
    return _keys;
}

#pragma mark - UITableViewDelegate & UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.keys.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSString * key = self.keys[section];
    return [self.sortedDic[key] count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 90;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString * key = self.keys[section];
    return key;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30;
}

- (NSArray<NSString *> *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    return self.keys;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HomeCell * cell = [tableView dequeueReusableCellWithIdentifier:homeCellId forIndexPath:indexPath];
    NSString * key = self.keys[indexPath.section];
    NSArray * array = self.sortedDic[key];
    Cards * cardModel = array[indexPath.row];
    cell.cardModel = cardModel;
    return cell;
}

@end
