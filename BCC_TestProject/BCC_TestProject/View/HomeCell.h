//
//  HomeCell.h
//  BCC_TestProject
//
//  Created by lyb on 2018/11/14.
//  Copyright © 2018年 lyb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TestModel.h"

NS_ASSUME_NONNULL_BEGIN

static NSString * homeCellId = @"homeCell";
@interface HomeCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *logoView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *categoryLabel;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;

@property (nonatomic, strong) Cards * cardModel;

@end

NS_ASSUME_NONNULL_END
