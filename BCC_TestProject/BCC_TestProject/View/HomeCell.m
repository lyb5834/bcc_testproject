//
//  HomeCell.m
//  BCC_TestProject
//
//  Created by lyb on 2018/11/14.
//  Copyright © 2018年 lyb. All rights reserved.
//

#import "HomeCell.h"

@implementation HomeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.logoView.layer.masksToBounds = YES;
    self.logoView.layer.cornerRadius = 4;
}

- (void)setCardModel:(Cards *)cardModel
{
    _cardModel = cardModel;
    if (cardModel.img.length > 0) {
        [self.logoView sd_setImageWithURL:[NSURL URLWithString:cardModel.img]];
    }else {
        self.logoView.image = [self textImageWithString:cardModel.lastName size:self.logoView.bounds.size];
    }
    
    self.titleLabel.text = [NSString stringWithFormat:@"%@%@",_cardModel.lastName,_cardModel.firstName];
    self.categoryLabel.text = _cardModel.title;
    self.descLabel.text = _cardModel.company;
}

- (UIImage *)textImageWithString:(NSString *)string size:(CGSize)size
{
    
    UIGraphicsBeginImageContextWithOptions(size, NO, [UIScreen mainScreen].scale);
    UIBezierPath * bezierPath = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, size.width, size.height) cornerRadius:0];
    [[UIColor groupTableViewBackgroundColor] setFill];
    [bezierPath fill];
    
    CGSize textSize = [self sizeWithText:string font:30 MaxWidth:100 MaxHeight:100];
    [string drawAtPoint:CGPointMake((size.width - textSize.width) / 2, (size.height - textSize.height) / 2) withAttributes:@{NSFontAttributeName : FONT(30), NSForegroundColorAttributeName : [UIColor darkGrayColor]}];
    UIImage * image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

- (CGSize)sizeWithText:(NSString *)text font:(CGFloat)font MaxWidth:(CGFloat)maxWidth MaxHeight:(CGFloat)maxHeight
{
    CGSize size;
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = NSLineBreakByCharWrapping;
    UIFont *Font = FONT(font);
    NSDictionary *dict = @{NSFontAttributeName:Font,NSParagraphStyleAttributeName:paragraphStyle};
    size = [text boundingRectWithSize:CGSizeMake(maxWidth, maxHeight) options:
            NSStringDrawingUsesLineFragmentOrigin|
            NSStringDrawingTruncatesLastVisibleLine|
            NSStringDrawingUsesFontLeading attributes:dict context:nil].size;
    return size;
}

@end
