//
//  TestModel.h
//  BCC_TestProject
//
//  Created by lyb on 2018/11/14.
//  Copyright © 2018年 lyb. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Sns :NSObject

@end

@interface WorkInfo :NSObject

@end

@interface Cards :NSObject
@property (nonatomic , copy) NSString              * verifyTime;
@property (nonatomic , assign) NSInteger              id;
@property (nonatomic , assign) BOOL              targetStore;
@property (nonatomic , copy) NSString              * backpicurl;
@property (nonatomic , copy) NSString              * groupName;
@property (nonatomic , copy) NSString              * img;
@property (nonatomic , copy) NSString              * picurl;
@property (nonatomic , copy) NSString              * title;
@property (nonatomic , copy) NSString              * department;
@property (nonatomic , copy) NSString              * relCompany;
@property (nonatomic , copy) NSString              * url;
@property (nonatomic , assign) BOOL              companyCard;
@property (nonatomic , copy) NSString              * company;
@property (nonatomic , copy) NSString              * lastNameEn;
@property (nonatomic , assign) BOOL              store;
@property (nonatomic , copy) NSString              * address;
@property (nonatomic , assign) NSInteger              cardType;
@property (nonatomic , copy) NSString              * desc;
@property (nonatomic , copy) NSString              * weibo;
@property (nonatomic , assign) NSInteger              star;
@property (nonatomic , copy) NSString              * im;
@property (nonatomic , copy) NSString              * phoneHome;
@property (nonatomic , copy) NSString              * school;
@property (nonatomic , copy) NSString              * groupId;
@property (nonatomic , copy) NSString              * signature;
@property (nonatomic , copy) NSString              * emailWork;
@property (nonatomic , assign) BOOL              update;
@property (nonatomic , copy) NSString              * lastName;
@property (nonatomic , copy) NSString              * starTimestamp;
@property (nonatomic , copy) NSString              * qq;
@property (nonatomic , copy) NSString              * localCardId;
@property (nonatomic , copy) NSString              * email;
@property (nonatomic , copy) NSString              * phoneOther;
@property (nonatomic , strong) Sns              * sns;
@property (nonatomic , copy) NSString              * industry;
@property (nonatomic , assign) NSInteger              verifyId;
@property (nonatomic , copy) NSString              * emailPrivate;
@property (nonatomic , copy) NSString              * birthday;
@property (nonatomic , copy) NSString              * firstName;
@property (nonatomic , copy) NSString              * firstNameEn;
@property (nonatomic , assign) NSInteger              targetId;
@property (nonatomic , copy) NSString              * phoneCompany;
@property (nonatomic , assign) NSInteger              updateTime;
@property (nonatomic , assign) BOOL              myStore;
@property (nonatomic , copy) NSString              * emailOther;
@property (nonatomic , copy) NSString              * timeStamp;
@property (nonatomic , assign) NSInteger              sourceType;
@property (nonatomic , copy) NSString              * middleNameEn;
@property (nonatomic , assign) NSInteger              gender;
@property (nonatomic , copy) NSArray<WorkInfo *>              * workInfo;
@property (nonatomic , copy) NSString              * city;
@property (nonatomic , assign) NSInteger              reqStatus;
@property (nonatomic , copy) NSString              * weixin;
@property (nonatomic , assign) NSInteger              createTime;
@property (nonatomic , copy) NSString              * mobile;
@property (nonatomic , copy) NSString              * phoneFax;
@property (nonatomic , copy) NSString              * phoneIphone;
@property (nonatomic , copy) NSString              * picIdentify;
@property (nonatomic , assign) NSInteger              privacy;
@property (nonatomic , copy) NSString              * website;

@end

@interface Data :NSObject
@property (nonatomic , assign) NSInteger              timestamp;
@property (nonatomic , copy) NSArray<Cards *>              * cards;
@property (nonatomic , assign) NSInteger              count;
@property (nonatomic , assign) NSInteger              pos;
@property (nonatomic , assign) NSInteger              total;

@end

@interface TestModel : NSObject

@property (nonatomic , copy) NSString              * message;
@property (nonatomic , strong) Data              * data;
@property (nonatomic , assign) NSInteger              status;

@end


NS_ASSUME_NONNULL_END
