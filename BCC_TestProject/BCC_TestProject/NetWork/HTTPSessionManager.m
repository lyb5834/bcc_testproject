//
//  HTTPSessionManager.m
//  BCC_TestProject
//
//  Created by lyb on 2018/11/14.
//  Copyright © 2018年 lyb. All rights reserved.
//

#import "HTTPSessionManager.h"

@implementation HTTPSessionManager

+ (nullable instancetype)sharedInstance
{
    static dispatch_once_t onceToken;
    static HTTPSessionManager *manager = nil;
    dispatch_once(&onceToken, ^{
        manager = [[HTTPSessionManager alloc] init];
        manager.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    });
    return manager;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.requestSerializer = [AFJSONRequestSerializer serializer];
        self.responseSerializer = [AFJSONResponseSerializer serializer];
        self.requestSerializer.timeoutInterval = 15.;
        self.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    }
    return self;
}

- (void)getWithURL:(NSString *)url success:(void (^) (NSURLSessionDataTask * task,id responseBody))success failure:(void (^) (NSURLSessionDataTask * task,NSError * error))failure
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [self GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        SAFE_BLOCK(success,task,responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        SAFE_BLOCK(failure,task,error);
    }];
}

@end
