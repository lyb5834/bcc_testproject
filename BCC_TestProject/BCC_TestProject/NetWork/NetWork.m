//
//  NetWork.m
//  BCC_TestProject
//
//  Created by lyb on 2018/11/14.
//  Copyright © 2018年 lyb. All rights reserved.
//

#import "NetWork.h"
#import "HTTPSessionManager.h"

@implementation NetWork

+ (void)getTestDataWithPage:(int)page
                       size:(int)size
                    success:(void (^) (id responseBody))success
                       fail:(void (^) (NSError * error))fail
{
    NSString * url = [NSString stringWithFormat:@"http://mobiletest.jingwei.com/querysection?userId=14085682&token=30e4dc2396db048c12c598096428ab0d&timestamp=%d&size=%d",page,size];
    [[HTTPSessionManager sharedInstance] getWithURL:url success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseBody) {
        SAFE_BLOCK(success,responseBody);
    } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
        SAFE_BLOCK(fail,error);
    }];
}

@end
