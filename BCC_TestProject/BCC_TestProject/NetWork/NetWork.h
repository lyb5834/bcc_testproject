//
//  NetWork.h
//  BCC_TestProject
//
//  Created by lyb on 2018/11/14.
//  Copyright © 2018年 lyb. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NetWork : NSObject

+ (void)getTestDataWithPage:(int)page
                       size:(int)size
                    success:(void (^) (id responseBody))success
                       fail:(void (^) (NSError * error))fail;

@end

NS_ASSUME_NONNULL_END
