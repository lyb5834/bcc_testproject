//
//  HTTPSessionManager.h
//  BCC_TestProject
//
//  Created by lyb on 2018/11/14.
//  Copyright © 2018年 lyb. All rights reserved.
//

#import "AFHTTPSessionManager.h"

NS_ASSUME_NONNULL_BEGIN

@interface HTTPSessionManager : AFHTTPSessionManager

+ (nullable instancetype)sharedInstance;

- (void)getWithURL:(NSString *)url success:(void (^) (NSURLSessionDataTask * task,id responseBody))success failure:(void (^) (NSURLSessionDataTask * task,NSError * error))failure;

@end

NS_ASSUME_NONNULL_END
